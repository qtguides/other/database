#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    databaseConnect();

    QStringList list = db.tables(QSql::Tables);
    if(list.count() == 0) // Database ya inicializado
    {
        databaseInit();
        databasePopulate();
    }
    databaseShow();
    db.close();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::databaseConnect()
{
    const QString DRIVER("QSQLITE");

    if(QSqlDatabase::isDriverAvailable(DRIVER))
    {
        db = QSqlDatabase::addDatabase(DRIVER);
        db.setDatabaseName("db.sqlite");
        //db.setDatabaseName(":memory:");
        if(!db.open())
            qWarning() << __FUNCTION__ << " ERROR: " << db.lastError().text();
    }
    else
    {
        qWarning() << __FUNCTION__ << " ERROR: no driver " << DRIVER << " available";
    }
}

void MainWindow::databaseInit()
{
    QSqlQuery query;

    if( !query.exec("CREATE TABLE persona (id INTEGER PRIMARY KEY, nombre VARCHAR(50), edad INTEGER)"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }
}

void MainWindow::databasePopulate()
{
    QSqlQuery query;

    if(    !query.exec("INSERT INTO persona(nombre, edad) VALUES('Martin Karadagian', 69)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Ramon Valdez', 64)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Pierre Nodoyuna', 45)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Dana Scully', 53)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Jason Bourne', 48)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Clark Kent', 34)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Mel Gibson', 63)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Jon Snow', 28)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Linus Torvalds', 49)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Michael Moore', 64)")
        || !query.exec("INSERT INTO persona(nombre, edad) VALUES('Sancho Panza', 51)"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }
}

void MainWindow::databaseShow()
{
    QSqlQuery query;
    QString s;

    if(!query.exec("SELECT id, nombre, edad FROM persona"))
    {
        qDebug() << "No se puede ejecutar Query !";
        return;
    }

    s = "<html><head/><body><p><table width='80%' style='border-spacing:0;border-style:solid;border-width:2px;border-color:green;'>";
    while(query.next())
    {
        s += "<tr>";
        s += "<td width='20%'><b><span style='color:#ff0000;'>" + query.value("id").toString() + "</span></b></td>";
        s += "<td width='60%'>" + query.value(1).toString() + "</td>";
        s += "<td width='20%'>" + query.value("edad").toString() + "</td>";
        s += "</tr>";
    }
    s += "</table></p></body></html>";
    ui->label->setText(s);
}
